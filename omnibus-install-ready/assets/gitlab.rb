external_url 'http://gitlab.internal'
unicorn['enable'] = false
puma['enable'] = true
# puma['worker_processes'] = 4 (probably don't need to change this for docker)
puma['max_threads'] = 4
puma['listen'] = '0.0.0.0'
gitlab_rails['initial_root_password'] = '5iveL!fe'
## NFS
gitlab_rails['uploads_directory'] = '/mnt/gitlab-nfs/uploads'
gitlab_rails['shared_path'] = '/mnt/gitlab-nfs/shared'
gitlab_ci['builds_directory'] = '/mnt/gitlab-nfs/builds'
node_exporter['listen_address'] = '0.0.0.0:9100'
gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0']
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'
# Components Config
## Redis - Combined Queues (Internal)
redis['enable'] = true
redis_exporter['listen_address'] = '0.0.0.0:9121'
## Sidekiq (Internal)
sidekiq['enable'] = true
sidekiq['listen_address'] = '0.0.0.0'
## Postgres \ Database (Internal)
postgresql['enable'] = true
postgresql['trust_auth_cidr_addresses'] = %w(0.0.0.0/0)
postgres_exporter['listen_address'] = '0.0.0.0:9187'
## Gitaly (Internal)
gitaly['enable'] = true